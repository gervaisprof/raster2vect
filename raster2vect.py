"""Main module of the raster2vect project.

. Transformation of a (known by PIL) bitmap image into list of lists of 1/0 pix.
    `pix = imgfile2tab(filename)`

. Compute outlines on the coordinates grid from such a list of lists of 1/0 pix.
    `outl = outlines_from_pixels(pix)`
    
. Simplify these outlines with Douglas-Peucker algorithm.
    `outl = douglas_peucker_list_simplify(outl, THRESHOLD)

. Transform the result into a vector image file.
    `save_eps_from_outlines(outl, filename+"eps")`
    `save_svg_from_outlines(outl, filename+"svg")`
"""

# Using get_current_dir(), cause os.getcwd() doesn't work within some IDEs
import inspect, os.path 
def get_current_dir():
    """Return current directory, ended by '/' (works even when launched by IDE).
    """
    # Thanks to 
    # https://stackoverflow.com/questions/2632199/
    #              how-do-i-get-the-path-of-the-current-executed-file-in-python
    #
    # Needs import inspect, os.path
    filename = inspect.getframeinfo(inspect.currentframe()).filename
    path = os.path.dirname(os.path.abspath(filename))
    return path + os.sep
os.chdir(get_current_dir())

from pix2outlines import outlines_from_pixels
from outlines2vect import save_eps_from_outlines, save_svg_from_outlines
from dpsimplify import douglas_peucker_outlines_simplify, stats
from imgfile2tab import imgfile2tab


if __name__ == "__main__":   

    # ex1.png"  butterfly.png  love_tree.png  wings.png dahlia.png...
    FILE = "france_map.png"
    THRESHOLD = 0# 2.5 #0.9
    STATS = True
    
    shortname = FILE[:-4]
    filename = get_current_dir() + "img/" + FILE

    # Get list of lists of pixels
    img = imgfile2tab(filename)
    # Compute outlines
    outl = outlines_from_pixels(img)
    if STATS: n1 = stats(outl)
    # Simplify outlines
    outl = douglas_peucker_outlines_simplify(outl, THRESHOLD)
    
    if STATS:
        print("Before:", n1, "points")
        n2 = stats(outl)
        print("After, with ", THRESHOLD, " threshold : ", n2 , " points (",
              "{:.1f}%".format(100 * (n2-n1) / n1), ")", sep="", end="\n\n")
    
    # Transform to vector image and save it to file
    filename = get_current_dir() + "output/" + shortname + ".eps"
    save_eps_from_outlines(outl, filename, dim=None)
    
    filename = get_current_dir() + "output/" + shortname + ".svg"
    save_svg_from_outlines(outl, filename, dim=None, filled=True)
    
    print("Ok, just have a look at", shortname +".eps and",
          shortname +".svg files in output/ local directory...")
