# raster2vect
Python3 project   
Convert **raster black & white image** (grid of 1/0 pixels) **into vector image** 
 (EPS, SVG formats)

*  `raster2vect.py` is the main file
*  `imgfile2tab.py` => transform a image file (e.g. `.png`,
inf fact any PIL-known format) into list of list of binary pixels (1 for black)  
like `[[0, 0, 1], [1, 1, 0]]`, say `pix`
*  `pix2outlines.py` => transform `pix` into `outlines`,   
where `outlines` is a list of outlines = a list of lists of points (= 2-tuples)   
like `[[(3, 0), (3, 2), (3, 0)], [(5, 7), ...], ...]`
*  `dpsimplify.py` => Douglas-Peucker segment-simplification of `outlines`,   
considering a given threshold.
*  `outlines2vect.py` => create EPS or SVG file from `outlines`.

For the moment, *maybe for ever*: 
+ no Bezier curves simplification
+ unfinished `.tex` (french only) documentation...
